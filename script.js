// FUNCTION 1
console.group('Task 1');

const restFromTwoNumbers = (x,y) => x/y;

console.log(restFromTwoNumbers(10,3))

console.groupEnd();

// FUNCTION 2
console.group('Task 2');

let x,y,z;

do {
  x = +prompt('Please enter the first number');
} while(isNaN(x));

do {
  y = +prompt('Please enter the second number');
} while(isNaN(y));

z = prompt('Please choose the operation: +, -, *, /')

if(z === '+' || z === '-' || z === '*' || z === '/') {
  alert('Press \'Ok\' and check the result in console!');
} else {
  alert ('Not appropriate operation');
}

function mathOperation (x,y,z) {
  if (z === '+'){
    return console.log(x+y);
  } else if (z === '-'){
    return console.log(x-y);
  } else if (z === '*') {
    return console.log(x*y);
  } else if (z === '/') {
    return console.log(x/y);
  } else {
    return 'Something went wrong';
  }
}

mathOperation(x,y,z);

console.groupEnd();

// FUNCTION 3
console.group('Task 3');

let int;

do {
  int = +prompt('Please enter the number to calculate a factorial');
  if (int === 0 || int === 1) {
    console.log(`Factorial for ${int} is always 1`);
  }
} while(isNaN(int));

const factorialCount = (int) => {
  let factorial = 1;

  for(let i = 1; i <= int; i++){
    factorial *= i;
  }

  return console.log(factorial);
}

factorialCount(int);

console.groupEnd()

